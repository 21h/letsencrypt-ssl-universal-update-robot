#!/bin/bash

cd "$(dirname "$0")"

./ssl_autorenew.sh > last_run.log

if [ $? -gt 0 ];
then
    cat last_run.log|/usr/bin/php ./sendmail.php -m admin@test.ru -f root@test.ru -s "ATTENTION! SSL RENEWAL FAILED!"
fi

