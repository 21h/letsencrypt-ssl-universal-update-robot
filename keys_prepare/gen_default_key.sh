#!/bin/bash
cd "$(dirname "$0")"
if [ -e ssl ];
then
    echo "READ IT CAREFULLY!"
    echo "Default key already exists! Remove it before generation new!"
    echo "REMEMBER! If you do it you MUST UPDATE PUB KEY ON ALL SERVERS!"
else
    ssh-keygen -f ssl -C "default SSL key" -N ""
fi
