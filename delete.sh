#!/bin/bash
cd "$(dirname "$0")"
echo "DELETE DOMAIN FROM SSL AUTORENEW"
echo "----------------------------------------"
echo "Enter domain name"
read domain
echo "Are you sure? [y/n]"
read yesactivate

if [ "$yesactivate" == "y" ];
then
    find . -name "$domain*" -exec rm -f {} \;  -print
    certbot delete -d $domain
fi
