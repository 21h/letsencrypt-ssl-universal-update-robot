#!/bin/bash
cd "$(dirname "$0")"
echo "ADD DOMAIN TO SSL AUTORENEW"
echo "----------------------------------------"
echo "Enter domain name"
read domain
echo "Enter remote command to restart webserver"
echo "typical: systemctl nginx restart"
read cmd
echo "Use default key 'ssl'? [y\n]"
read defaultkey
echo "Start update process of all SSL certifcates right now? [y\n]"
read activate

touch domains/$domain
echo "$cmd" > cmds/$domain
if [ "$defaultkey" == "y" ];
then
    ln -s ../keys_prepare/ssl keys/$domain.pem
else
    echo "Add your key to 'keys' directory and chmod 600"
    echo "i.e. keys/mydomain.com.pem."
    echo "*.pem extension required! chmod 600 on your key required!"
fi

if [ "$activate" == "y" ];
then
    ./ssl-autorenew.sh
fi
