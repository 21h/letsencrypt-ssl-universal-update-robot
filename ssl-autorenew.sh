#!/bin/bash

cd "$(dirname "$0")"

is_error=0
error_text=""

date
echo "SSL Renew started "

# Renew or get new certificates, copy them to remote hosts and restart webserver
for domain in $(ls -w1 domains) 
do
    # Get or renew
    echo "$domain: last update: $(cat domains/$domain)"
    certbot certonly -n -c config.ini --standalone --preferred-challenges http --domain $domain
    if [ $? -gt 0 ];
    then
	is_error=1
	error_text="Certificate renewal error!"
	echo "$error_text"
	exit 1
    fi
    # Copy updated certs to remote host
    ssh-keygen -R $domain
    ssh-keyscan -4 -H $domain >> ~/.ssh/known_hosts
    ssh -i ./keys/$domain.pem root@$domain "mkdir -p /etc/ssl/web/$domain"
    if [ $? -gt 0 ];
    then
	is_error=1
	error_text="Can not create remote certificate directory"
	echo "$error_text"
	exit 1
    fi
    scp -i ./keys/$domain.pem -r /etc/letsencrypt/live/$domain/* root@$domain:/etc/ssl/web/$domain/
    if [ $? -gt 0 ];
    then
	is_error=1
	error_text="Can not copy certificate to remote server"
	echo "$error_text"
	exit 1
    fi
    # Restart remote webserver
    ssh -i ./keys/$domain.pem root@$domain "$(cat cmds/$domain)"
    if [ $? -gt 0 ];
    then
	is_error=1
	error_text="Can not restart remote server. Server down!"
	echo "$error_text"
	exit 1
    fi
    echo "$domain: updating last update record"
    date > domains/$domain
done

echo "SSL Renew stopped"
date

